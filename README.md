# gitmoji-lite

[![](https://shields.kaki87.net/website.svg?url=https://gitmoji.kaki87.net)](https://gitmoji.kaki87.net)
![](https://shields.kaki87.net/badge/license-MIT-green)
[![](https://shields.kaki87.net/badge/512KB%20Club-Orange%20team-fbc02d)](https://github.com/kevquirk/512kb.club/blob/main/_data/sites.yml#L1241)

An emoji guide for your commit messages.

## Differences with the [original Gitmoji project by carloscuesta](https://github.com/carloscuesta/gitmoji)

### Ad-free

The original project's website contains ads since [PR #631](https://github.com/carloscuesta/gitmoji/pull/631).

This project's website does not contain ads nor any other annoyance.

### Framework-less

The original project's website uses NextJS since [PR #368](https://github.com/carloscuesta/gitmoji/pull/368), and was using GulpJS before that since [commit `f2a567d`](https://github.com/carloscuesta/gitmoji/commit/f2a567de553f7f41e05dd7b3693d637387e7ce5d).

This project's website does not use any framework but only pure HTML/CSS/JS.

### Unofficial gitmojis

The original project's maintainers sometimes reject gitmoji submissions, despite respecting the [contributing guidelines](https://github.com/carloscuesta/gitmoji/blob/master/.github/CONTRIBUTING.md#how-to-add-a-gitmoji), or let submissions unsolved forever, even after discussion and approval.

This project's website uses the same gitmoji list as the original, but also adds the following rejected gitmojis, with an *unofficial* badge.

- `:fast_forward:` `Redo changes` ([issue #169](https://github.com/carloscuesta/gitmoji/issues/169), rejected)
- `:newspaper:` `Write user-facing content` ([issue #1726](https://github.com/carloscuesta/gitmoji/issues/1726), unsolved)

Unofficial gitmoji submissions are welcome, provided that an existing submission has been rejected or left unsolved on
the official project's issue tracker, despite respecting the contributing guidelines.